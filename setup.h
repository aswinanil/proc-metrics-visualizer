#ifndef SETUP_H
#define SETUP_H

#include <QWidget>
#include <widget.h>

namespace Ui {
class Setup;
}

class Setup : public QWidget
{
    Q_OBJECT

public:
    explicit Setup(QWidget *parent = 0);
    ~Setup();

private slots:

    void on_pushButton_binary_clicked();

    void on_pushButton_Argms_clicked();

    void on_pushButton_generateData_clicked();

    void on_pushButton_visualize_clicked();

    void readFromStdOutScript();

    void readFromStdErrorScript();

    void dataGenerated();

    void on_pushButton_existingData_clicked();

private:

    Ui::Setup *ui;

    QFileInfo binaryFileInfo;

    QFileInfo dataFileInfo;

    QProcess *processScript;

    QStringList cmdLineArgms;
};

#endif
