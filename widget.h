#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <qwt_plot.h>
#include <qwt_legend.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_zoomer.h>
#include <qfile.h>
#include <qmessagebox.h>
#include <qsignalmapper.h>
#include <qwt_picker.h>
#include <qwt_picker_machine.h>
#include <qprocess.h>
#include <qbytearray.h>
#include <qwt_plot_marker.h>
#include <qwt_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_scale_widget.h>
#include <QFileDialog>
#include <QDebug>
#include <QException>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    void setupGDB(QFileInfo binaryFileInfo);
    void setupWidget();
    void setDataFileLocation(QFileInfo dataFileInfo);
    ~Widget();

private slots:

    void on_mouse_click(const QPointF &pos);

    void on_mouse_click2(const QPointF &pos);

    void on_mouse_click3(const QPointF &pos);

    void on_rect_dragged(const QRectF &rect);

    void readFromStdOut();

    void readFromStdError();

    void on_pushButton_Reset_clicked();

    void on_radioButton_click_clicked(bool checked);

    void on_radioButton_drag_clicked(bool checked);

    void on_pushButton_getSource_clicked();

public slots:


private:
    Ui::Widget *ui;

    bool global_ZoomingPlot3;
    double global_PcCycleMax;
    QProcess *process;
    QFileInfo dataFileInfo;
    qlonglong pcAddressSeperator;

    QwtPlot plot1;
    QwtPlot plot2;
    QwtPlot plot3;

    QwtPlotCurve * curvePCAddress;
    QVector<QPointF>* samplesPCAddress;
    QVector<QPointF>* samplesMemory;
    QVector<QPointF>* samplesBranch;
    QVector<QPointF>* samplesArithmetic;
    QVector<QPointF>* samplesCacheMissL2Ratio;
    QVector<QPointF>* samplesCacheMissL1Ratio;
    QVector<QPointF>* samplesInsNormalized;
    QVector<QPointF>* samplesCacheMissTotalRatioNormalized;

    QwtPlotPicker *plotPickerRect;
    QwtPlotPicker *plotPickerRect2;
    QwtPlotPicker *plotPickerRect3;

    QwtPlotMarker *plotMarkerPCAddress;
    QwtPlotMarker *plotMarkerMemory;
    QwtPlotMarker *plotMarkerBranch;
    QwtPlotMarker *plotMarkerArithmetic;
    QwtPlotMarker *plotMarkerCacheMissL2Ratio;
    QwtPlotMarker *plotMarkerCacheMissL1Ratio;
    QwtPlotMarker *plotMarkerInsNormalized;
    QwtPlotMarker *plotMarkerCacheMissTotalRatioNormalized;
    QwtPlotMarker *plotMarker1;
    QwtPlotMarker *plotMarker2;
    QwtPlotMarker *plotMarker3;

    void setupPlot(QwtPlot *plot1, QwtPlot *plot2, QwtPlot *plot3);

    void drawPlot(QwtPlot *plot1, QwtPlot *plot2, QwtPlot *plot3);

    void parseFileGetCurvePoints(QVector<QPointF> *samplesPCAddressSeperator);

    qlonglong parseFileGetAddressMapping(QHash<QString, qlonglong> *hashPcAddressTagFull);

    int getClosestPointIndex(double x);
};

#endif // WIDGET_H
