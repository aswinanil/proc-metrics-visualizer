#-------------------------------------------------
#
# Project created by QtCreator 2014-05-22T13:27:02
#
#-------------------------------------------------

include ( /usr/local/qwt-6.1.0/features/qwt.prf )

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProcessMetricsVisualTool
TEMPLATE = app


SOURCES += main.cpp\
    widget.cpp \
    setup.cpp

HEADERS  += widget.h \
    setup.h

FORMS    += widget.ui \
    setup.ui
