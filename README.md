proc-metrics-visualizer
==================


##About
A visualization and analytics tool for process metrics with a focus on parsing huge amounts of data quickly and presenting them in an intuitive manner.

##Requirements
[QT framework](http://qt-project.org/) and [QWT](http://qwt.sourceforge.net/index.html).
In *.pro* file update the file path of *qwt.prf* file referenced, if necessary.

Works best on linux.

##Usage
Only selection of pre-existing data is enabled in this version. Sample data is provided for this purpose. Generating new data via binaries requires interaction with a custom built module which is not uploaded and has thus been disabled.