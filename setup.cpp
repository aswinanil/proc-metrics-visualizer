#include "setup.h"
#include "ui_setup.h"

Setup::Setup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Setup)
{
    ui->setupUi(this);
}

Setup::~Setup()
{
    delete ui;
}

void Setup::on_pushButton_binary_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);

    if (dialog.exec()) {
        QStringList binaryFileName = dialog.selectedFiles();
        binaryFileInfo = QFileInfo(binaryFileName.at(0));
    }

    ui->pushButton_Argms->setEnabled(true);
}

void Setup::on_pushButton_Argms_clicked()
{
    cmdLineArgms.clear();

    // Open a text file
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);

    QStringList cmdLineArgmsFileName;

    if (dialog.exec()) {
        cmdLineArgmsFileName = dialog.selectedFiles();
    }

    QFile file(cmdLineArgmsFileName.at(0));

    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    QStringList splitBinaryArgms;
    QString line;

    if(!in.atEnd()) {
        line = in.readLine();
        line = line.trimmed();
    }

    file.close();

    // If splitBinaryArgms is empty add in a " " because the script file expects a third argument
    // Split contents by " "
    // Save it for running with the script

    cmdLineArgms.append("1");
    cmdLineArgms.append(binaryFileInfo.filePath());
    cmdLineArgms.append(line);

    ui->pushButton_generateData->setEnabled(true);
    ui->pushButton_visualize->setEnabled(true);
}

void Setup::readFromStdOutScript() {
    QString scriptOut(processScript->readAllStandardOutput());

    int scriptOutInt;
    if(scriptOut.length() == 1) {
        scriptOutInt = scriptOut.toInt();
        if(scriptOutInt != 0) {
            ui->progressBar->setValue(scriptOutInt * 10);
        }
    }

    ui->textBrowser->append("Run No. " + scriptOut + " completed..");

    qDebug() << scriptOut;
}

void Setup::readFromStdErrorScript() {
    QString scriptError(processScript->readAllStandardError());
    qDebug() << scriptError;
}

void Setup::dataGenerated() {
    ui->progressBar->setValue(100);
    ui->textBrowser->setText("Data generated! Please click the Visualize button to plot your graph.");
    ui->pushButton_visualize->setEnabled(true);
}

void Setup::on_pushButton_generateData_clicked()
{
    // Run the script file
    if(!binaryFileInfo.exists())  // Perhaps print error code as well
        return;

    ui->pushButton_binary->setEnabled(false);
    ui->pushButton_Argms->setEnabled(false);
    ui->pushButton_generateData->setEnabled(false);
    ui->pushButton_visualize->setEnabled(false);

    processScript = new QProcess(this);
    connect(processScript, SIGNAL(readyReadStandardOutput()), this, SLOT(readFromStdOutScript()) );
    connect(processScript, SIGNAL(readyReadStandardError()), this, SLOT(readFromStdErrorScript()) );
    connect(processScript, SIGNAL(finished(int)), this, SLOT(dataGenerated()) );

    // If there is a directory called new_output remove it
    QDir dir = QDir("new_output");
    if(dir.exists()) {
        dir.removeRecursively();
    }

    if(cmdLineArgms.isEmpty()) {
        cmdLineArgms.append("1");
        cmdLineArgms.append(binaryFileInfo.filePath());
        cmdLineArgms.append("");
    }

    ui->textBrowser->setText("Generating process metrics...");
    processScript->start("../plife/Runexperiment_470_fullrun_counter_ver3_bin_inp_modified_dir_flexible_cmd_line_progress_updated.sh", cmdLineArgms);
}

void Setup::on_pushButton_visualize_clicked()
{
    // I need to pass the binaryFileName so gdb in w can use it
    Widget* w = new Widget();
    w->setDataFileLocation(dataFileInfo);
    w->setupWidget();
    w->show();
    this->hide();
}

void Setup::on_pushButton_existingData_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly);

    if (dialog.exec()) {
        QStringList existingDataFileName = dialog.selectedFiles();
        dataFileInfo = QFileInfo(existingDataFileName.at(0));
    }

    if(dataFileInfo.isDir()) {
        ui->pushButton_binary->setEnabled(false);
        ui->pushButton_visualize->setEnabled(true);
    }
}
