#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

void Widget::setupWidget() {
    setupPlot(&plot1, &plot2, &plot3);
    ui->horizontalLayout_canvas1->addWidget(&plot1);
    ui->horizontalLayout_canvas2->addWidget(&plot2);
    ui->horizontalLayout_canvas3->addWidget(&plot3);
    drawPlot(&plot1, &plot2, &plot3);
}

void Widget::setDataFileLocation(QFileInfo dataFileInfo) {
    this->dataFileInfo.setFile(dataFileInfo.filePath());
}

void Widget::setupGDB(QFileInfo binaryFileInfo) {
    process = new QProcess(this);
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readFromStdOut())  );
    connect(process, SIGNAL(readyReadStandardError()), this, SLOT(readFromStdError())  );

    process->start("gdb");
    if(!process->waitForStarted())
        return;

    process->write(QString("cd " + binaryFileInfo.absolutePath()).toLocal8Bit());
    process->write("\n");

    process->write(QString("file " + binaryFileInfo.fileName()).toLocal8Bit());
    process->write("\n");

    process->write("set listsize 1");
    process->write("\n");
}

void Widget::readFromStdOut() {
    QString gdbOut(process->readAllStandardOutput());
    ui->textBrowserGDBOutput->setText(gdbOut);
}

void Widget::readFromStdError() {
    QString gdbError(process->readAllStandardError());
    ui->textBrowserGDBOutput->setText(gdbError);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setupPlot(QwtPlot *plot1, QwtPlot *plot2, QwtPlot *plot3) {
    plot1->setTitle("Instruction Profile With IPC");
    plot1->setCanvasBackground(QColor(Qt::white));
    plot1->setAutoReplot(false);

    plot2->setTitle("L1 L2 Cache Profile with %TotalCacheMiss");
    plot2->setCanvasBackground(QColor(Qt::white));
    plot2->setAutoReplot(false);

    plot3->setTitle("PC Addr Tag Relative to Address Space");
    plot3->setCanvasBackground(QColor(Qt::white));
    plot3->setAutoReplot(false);

    // legend
    QwtLegend *legend1 = new QwtLegend;
    legend1->setFrameStyle(QFrame::Box|QFrame::Sunken);
    plot1->insertLegend(legend1, QwtPlot::BottomLegend);
    QwtLegend *legend2 = new QwtLegend;
    legend2->setFrameStyle(QFrame::Box|QFrame::Sunken);
    plot2->insertLegend(legend2, QwtPlot::BottomLegend);
    QwtLegend *legend3 = new QwtLegend;
    legend3->setFrameStyle(QFrame::Box|QFrame::Sunken);
    plot3->insertLegend(legend3, QwtPlot::BottomLegend);

    // axis
    plot3->setAxisTitle(QwtPlot::xBottom, "Cycles");
    plot3->setMaximumHeight(220);

    plot1->setAxisTitle(QwtPlot::xBottom, "Cycles");
    plot1->setMaximumHeight(220);
    plot1->axisScaleDraw(QwtPlot::yLeft)->setMinimumExtent(64);

    plot2->setAxisTitle(QwtPlot::xBottom, "Cycles");
    plot2->setMaximumHeight(220);    
    plot2->axisScaleDraw(QwtPlot::yLeft)->setMinimumExtent(64);

    QwtPlotPicker *plotPicker = new QwtPlotPicker(plot1->canvas());
    plotPicker->setStateMachine(new QwtPickerClickPointMachine());

    QwtSymbol *sym = new QwtSymbol(QwtSymbol::Diamond,QBrush(Qt::blue),QPen(Qt::blue),QSize(5,5));
    plotMarkerMemory = new QwtPlotMarker("Memory");
    plotMarkerMemory->setSymbol(sym);

    plotMarkerBranch = new QwtPlotMarker("Branch");
    plotMarkerBranch->setSymbol(sym);

    plotMarkerArithmetic = new QwtPlotMarker("Arithmetic");
    plotMarkerArithmetic->setSymbol(sym);

    plotMarkerInsNormalized = new QwtPlotMarker("InsNormalized");
    plotMarkerInsNormalized->setSymbol(sym);

    plotMarkerCacheMissL1Ratio = new QwtPlotMarker("CacheMissL1Ratio");
    plotMarkerCacheMissL1Ratio->setSymbol(sym);

    plotMarkerCacheMissL2Ratio = new QwtPlotMarker("CacheMissL2Ratio");
    plotMarkerCacheMissL2Ratio->setSymbol(sym);

    plotMarkerCacheMissTotalRatioNormalized = new QwtPlotMarker("CacheMissTotalRatioNormalized");
    plotMarkerCacheMissTotalRatioNormalized->setSymbol(sym);

    plotMarkerPCAddress = new QwtPlotMarker("PCAddress");
    plotMarkerPCAddress->setSymbol(sym);

    plotMarker1 = new QwtPlotMarker("1st Plot");
    plotMarker1->setLinePen(Qt::blue, 2.0, Qt::DashLine);
    plotMarker1->setLineStyle(QwtPlotMarker::VLine);

    plotMarker2 = new QwtPlotMarker("2nd Plot");
    plotMarker2->setLinePen(Qt::blue, 2.0, Qt::DashLine);
    plotMarker2->setLineStyle(QwtPlotMarker::VLine);

    plotMarker3 = new QwtPlotMarker("3rd Plot");
    plotMarker3->setLinePen(Qt::blue, 2.0, Qt::DashLine);
    plotMarker3->setLineStyle(QwtPlotMarker::VLine);

    plotMarker1->attach(plot1);
    plotMarker2->attach(plot2);
    plotMarker3->attach(plot3);

    connect( plotPicker, SIGNAL( selected( const QPointF & ) ), this, SLOT(on_mouse_click( const QPointF & )) );

    QwtPlotPicker *plotPicker2 = new QwtPlotPicker(plot2->canvas());
    plotPicker2->setStateMachine(new QwtPickerClickPointMachine());
    connect( plotPicker2, SIGNAL( selected( const QPointF & ) ), this, SLOT(on_mouse_click2( const QPointF & )) );

    QwtPlotPicker *plotPicker3 = new QwtPlotPicker(plot3->canvas());
    plotPicker3->setStateMachine(new QwtPickerClickPointMachine());
    connect( plotPicker3, SIGNAL( selected( const QPointF & ) ), this, SLOT(on_mouse_click3( const QPointF & )) );

    plotPickerRect = new QwtPlotPicker(plot1->canvas());
    plotPickerRect->setStateMachine(new QwtPickerDragRectMachine());
    plotPickerRect->setRubberBand(QwtPicker::RectRubberBand);
    connect( plotPickerRect, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );


    plotPickerRect2 = new QwtPlotPicker(plot2->canvas());
    plotPickerRect2->setStateMachine(new QwtPickerDragRectMachine());
    plotPickerRect2->setRubberBand(QwtPicker::RectRubberBand);
    connect( plotPickerRect2, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );

    plotPickerRect3 = new QwtPlotPicker(plot3->canvas());
    plotPickerRect3->setStateMachine(new QwtPickerDragRectMachine());
    plotPickerRect3->setRubberBand(QwtPicker::RectRubberBand);
    connect( plotPickerRect3, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );
}

void Widget::on_mouse_click(const QPointF &pos)
{
    if(ui->radioButton_drag->isChecked()) {
        return;
    }

    double x = pos.x();
    ui->textBrowserGDBOutput->setText("PC Cycle: " + QString::number(x));

    QPointF closestPoint;
    for(int i=0; i<samplesPCAddress->size(); ++i) {
        if(samplesPCAddress->at(i).x() >= x) {
            closestPoint = samplesPCAddress->at(i);
            break;
        }
    }

    int i = getClosestPointIndex(x);

    double y2 = samplesBranch->at(i).y();
    double y3 = samplesArithmetic->at(i).y();
    double y4 = samplesInsNormalized->at(i).y();
    double y5 = samplesCacheMissL1Ratio->at(i).y();
    double y6 = samplesCacheMissL2Ratio->at(i).y();
    double y7 = samplesCacheMissTotalRatioNormalized->at(i).y();
    double y8 = samplesPCAddress->at(i).y();

    plotMarkerBranch->setValue(x, y2);
    plotMarkerArithmetic->setValue(x, y3);
    plotMarkerInsNormalized->setValue(x, y4);
    plotMarkerCacheMissL1Ratio->setValue(x, y5);
    plotMarkerCacheMissL2Ratio->setValue(x, y6);
    plotMarkerCacheMissTotalRatioNormalized->setValue(x, y7);
    plotMarkerPCAddress->setValue(x, y8);

    plotMarker1->setXValue(x);
    plotMarker2->setXValue(x);
    plotMarker3->setXValue(x);

    plot1.replot();
    plot2.replot();
    plot3.replot();
}

void Widget::on_mouse_click2(const QPointF &pos)
{
    if(ui->radioButton_drag->isChecked()) {
        return;
    }
    double x = pos.x();
    ui->textBrowserGDBOutput->setText("PC Cycle: " + QString::number(x));

    QPointF closestPoint;
    for(int i=0; i<samplesPCAddress->size(); ++i) {
        if(samplesPCAddress->at(i).x() >= x) {          // Can be optimised
            closestPoint = samplesPCAddress->at(i);
            break;
        }
    }

    plotMarker1->setXValue(x);
    plotMarker2->setXValue(x);
    plotMarker3->setXValue(x);

    plot1.replot();
    plot2.replot();
    plot3.replot();
}

void Widget::on_mouse_click3(const QPointF &pos)
{
    if(ui->radioButton_drag->isChecked()) {
        return;
    }
    double x = pos.x();
    ui->textBrowserGDBOutput->setText("PC Cycle: " + QString::number(x));

    QPointF closestPoint;
    for(int i=0; i<samplesPCAddress->size(); ++i) {
        if(samplesPCAddress->at(i).x() >= x) {
            closestPoint = samplesPCAddress->at(i);
            break;
        }
    }

    plotMarker1->setXValue(x);
    plotMarker2->setXValue(x);
    plotMarker3->setXValue(x);


    plot1.replot();
    plot2.replot();
    plot3.replot();
}

void Widget::on_rect_dragged(const QRectF &rect)
{
    if(ui->radioButton_click->isChecked())
        return;

    QPointF posFirst = rect.topLeft();
    QPointF posLast = rect.bottomRight();

    qreal intervalX = posLast.x() - posFirst.x();
    qreal intervalY = posLast.y() - posFirst.y();

    if(intervalX == 0 || intervalY == 0)
        return;

    plot1.setAxisScale(QwtPlot::xBottom, posFirst.x(), posLast.x(), 0);
    plot2.setAxisScale(QwtPlot::xBottom, posFirst.x(), posLast.x(), 0);
    plot3.setAxisScale(QwtPlot::xBottom, posFirst.x(), posLast.x(), 0);

    plot1.replot();
    plot2.replot();
    plot3.replot();
}

void Widget::drawPlot(QwtPlot *plot1, QwtPlot *plot2, QwtPlot *plot3)
{
    QwtPlotCurve *curveMemory = new QwtPlotCurve("Memory Instructions Curve");
    QwtPlotCurve *curveBranch = new QwtPlotCurve("Branch Instructions Curve");
    QwtPlotCurve *curveArithmetic = new QwtPlotCurve("Arithmetic Instructions Curve");
    QwtPlotCurve *curveInsNormalized = new QwtPlotCurve("Normalised Instruction Curve");

    QwtPlotCurve *curveCacheMissTotalRatio = new QwtPlotCurve("Cache Miss Total Ratio Curve");
    QwtPlotCurve *curveCacheMissL2Ratio = new QwtPlotCurve("Cache Miss L2 Ratio Curve");
    QwtPlotCurve *curveCacheMissL1Ratio = new QwtPlotCurve("Cache Miss L1 Ratio Curve");

    curvePCAddress = new QwtPlotCurve("PC Address Curve");
    QwtPlotCurve * curvePCAddressSeperator = new QwtPlotCurve("PC Address Curve Seperator");

    curveArithmetic->setPen(QColor(Qt::yellow));
    curveArithmetic->setBrush(QBrush(Qt::yellow));
    curveBranch->setPen(QColor(Qt::green));
    curveBranch->setBrush(QBrush(Qt::green));
    curveMemory->setPen(QColor(Qt::red));
    curveMemory->setBrush(QBrush(Qt::red));

    curveCacheMissL2Ratio->setPen(QColor(Qt::red));
    curveCacheMissL2Ratio->setBrush(QBrush(Qt::red));
    curveCacheMissL1Ratio->setPen(QColor(Qt::green));
    curveCacheMissL1Ratio->setBrush(QBrush(Qt::green));

    curvePCAddress->setStyle(QwtPlotCurve::Dots);
    curvePCAddress->setPen(QColor(Qt::black), 1.5, Qt::DotLine);
    curvePCAddressSeperator->setPen(QColor(Qt::blue), 0.7, Qt::DashLine);  //setStyle(QwtPlotCurve::);

    QwtPointSeriesData* dataMemory = new QwtPointSeriesData;
    samplesMemory = new QVector<QPointF>;

    QwtPointSeriesData* dataBranch = new QwtPointSeriesData;
    samplesBranch = new QVector<QPointF>;

    QwtPointSeriesData* dataArithmetic = new QwtPointSeriesData;
    samplesArithmetic = new QVector<QPointF>;

    QwtPointSeriesData* dataInsNormalized = new QwtPointSeriesData;
    samplesInsNormalized = new QVector<QPointF>;

    QwtPointSeriesData* dataPCAddress = new QwtPointSeriesData;
    samplesPCAddress = new QVector<QPointF>;

    QwtPointSeriesData* dataPCAddressSeperator = new QwtPointSeriesData;
    QVector<QPointF>* samplesPCAddressSeperator = new QVector<QPointF>;

    QwtPointSeriesData* dataCacheMissTotalRatioNormalized = new QwtPointSeriesData;
    samplesCacheMissTotalRatioNormalized = new QVector<QPointF>;

    QwtPointSeriesData* dataCacheMissL2Ratio = new QwtPointSeriesData;
    samplesCacheMissL2Ratio = new QVector<QPointF>;

    QwtPointSeriesData* dataCacheMissL1Ratio = new QwtPointSeriesData;
    samplesCacheMissL1Ratio = new QVector<QPointF>;

    this->parseFileGetCurvePoints(samplesPCAddressSeperator);

    dataMemory->setSamples(*samplesMemory);
    dataBranch->setSamples(*samplesBranch);
    dataArithmetic->setSamples(*samplesArithmetic);
    dataInsNormalized->setSamples(*samplesInsNormalized);
    dataCacheMissTotalRatioNormalized->setSamples(*samplesCacheMissTotalRatioNormalized);
    dataCacheMissL2Ratio->setSamples(*samplesCacheMissL2Ratio);
    dataCacheMissL1Ratio->setSamples(*samplesCacheMissL1Ratio);
    dataPCAddress->setSamples(*samplesPCAddress);
    dataPCAddressSeperator->setSamples(*samplesPCAddressSeperator);

    curveArithmetic->setData(dataArithmetic);
    curveArithmetic->attach(plot1);
    curveBranch->setData(dataBranch);
    curveBranch->attach(plot1);
    curveMemory->setData(dataMemory);
    curveMemory->attach(plot1);
    curveInsNormalized->setData(dataInsNormalized);
    curveInsNormalized->attach(plot1);

    curveCacheMissL1Ratio->setData(dataCacheMissL1Ratio);
    curveCacheMissL1Ratio->attach(plot2);
    curveCacheMissL2Ratio->setData(dataCacheMissL2Ratio);
    curveCacheMissL2Ratio->attach(plot2);
    curveCacheMissTotalRatio->setData(dataCacheMissTotalRatioNormalized);
    curveCacheMissTotalRatio->attach(plot2);

    curvePCAddress->setData(dataPCAddress);
    curvePCAddress->attach(plot3);
    curvePCAddressSeperator->setData(dataPCAddressSeperator);
    curvePCAddressSeperator->attach(plot3);

    plot1->replot();
    plot2->replot();
    plot3->setAxisScale(QwtPlot::yLeft, 0, 130000000000000, 0);
    plot3->replot();
}

void Widget::parseFileGetCurvePoints(QVector<QPointF> *samplesPCAddressSeperator)
{
    QHash<QString, qlonglong> *hashPcAddressTagFull = new QHash<QString, qlonglong>;
    pcAddressSeperator = parseFileGetAddressMapping(hashPcAddressTagFull);

    QString dataFileLocation = dataFileInfo.absoluteFilePath() + "/papi_sampling_per_cycle_combined.csv";
    QFile file(dataFileLocation);

    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    double maxPcIns = 0, maxCacheMissTotalRatio = 0;
    QVector<double> *samplesIns = new QVector<double>;
    QVector<double> *samplesCacheMissTotalRatio = new QVector<double>;
    QVector<double> *samplesPcCycles = new QVector<double>;

    in.readLine();
    while(!in.atEnd()) {

        QString line = in.readLine();
        QStringList fields = line.split(",");

        if(fields.length() < 55)
            break;

        double pcCycles = fields[3].toDouble();
        double pcBranch = fields[51].toDouble() / fields[54].toDouble(); // Index 51 is pcBrIns, index 54 is pcIns;

        double cacheAccessTotal = fields[2].toDouble() + fields[5].toDouble();
        double cacheMissTotal = fields[37].toDouble();

        double pcMemory = cacheAccessTotal / fields[4].toDouble(); // Index 2 is pcLdIns, index 5 is pcSrIns, index 4 is pcUop
        double pcArithmetic = 1.0 - pcMemory - pcBranch;

        double pcIns = fields[54].toDouble();
        samplesIns->push_back(pcIns);
        if(maxPcIns < pcIns)
            maxPcIns = pcIns;

        double cacheMissL2 = fields[39].toDouble();
        double cacheMissTotalRatio = cacheMissTotal/cacheAccessTotal;

        if(maxCacheMissTotalRatio < cacheMissTotalRatio)
            maxCacheMissTotalRatio = cacheMissTotalRatio;

        double cacheMissL2Ratio = cacheMissL2/cacheMissTotal;
        double cacheMissL1Ratio = 1;

        QString pcAddressString = fields[1].remove('"');
        qlonglong pcAddress = hashPcAddressTagFull->value(pcAddressString);        
        qlonglong pcAddressModified = (pcAddressSeperator - pcAddress) / 2 + pcAddress;

        // For cumulative graphs
        pcBranch += pcMemory;
        pcArithmetic += pcBranch;  

        samplesCacheMissTotalRatio->push_back(cacheMissTotalRatio);
        samplesPcCycles->push_back(pcCycles);
        samplesMemory->push_back(QPointF(pcCycles, pcMemory));
        samplesBranch->push_back(QPointF(pcCycles, pcBranch));
        samplesArithmetic->push_back(QPointF(pcCycles, pcArithmetic));
        samplesPCAddress->push_back(QPointF(pcCycles, pcAddressModified));
        samplesPCAddressSeperator->push_back(QPointF(pcCycles, pcAddressSeperator));
        samplesCacheMissL2Ratio->push_back(QPointF(pcCycles, cacheMissL2Ratio));
        samplesCacheMissL1Ratio->push_back(QPointF(pcCycles, cacheMissL1Ratio));
    }

    for(int i=0; i<samplesIns->size(); ++i)
    {
        samplesInsNormalized->push_back(QPointF(samplesPcCycles->at(i), samplesIns->at(i)/maxPcIns));
        samplesCacheMissTotalRatioNormalized->push_back(QPointF(samplesPcCycles->at(i), samplesCacheMissTotalRatio->at(i)/maxCacheMissTotalRatio));
        if(i == samplesIns->size() - 1)
            global_PcCycleMax = samplesPcCycles->at(i);
    }

    file.close();
}

qlonglong Widget::parseFileGetAddressMapping(QHash<QString, qlonglong> *hashPcAddressTagFull)
{
    QString dataFileLocation = dataFileInfo.absoluteFilePath() + "/bb_hash_out.dat";
    QFile file(dataFileLocation);

    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    QList<qlonglong> samplesPcAddressFullList;

    in.readLine();
    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split(" ");

        QString pcAddressTag = fields[0];
        QString pcAddressFull = fields[1];
        bool ok;
        qlonglong pcAddressFullLongLong = pcAddressFull.toLongLong(&ok, 16);

        hashPcAddressTagFull->insert(pcAddressTag, pcAddressFullLongLong);
        samplesPcAddressFullList.append(pcAddressFullLongLong);
    }

    qSort(samplesPcAddressFullList);
    qlonglong maxDiff = 0;
    int maxDiffAfterIndex = 0;
    for(int i=0; i<samplesPcAddressFullList.size()-1; ++i)
    {
        qlonglong diff = samplesPcAddressFullList.at(i+1) - samplesPcAddressFullList.at(i);
        if(diff > maxDiff)
        {
            maxDiff = diff;
            maxDiffAfterIndex = i;
        }
    }

    file.close();
    return samplesPcAddressFullList.at(maxDiffAfterIndex) + maxDiff/2;
}

int Widget::getClosestPointIndex(double x)
{
    int i;
    for(i=0; i<samplesPCAddress->size(); ++i) {
        if(samplesPCAddress->at(i).x() >= x) {
            return i;
        }
    }
    return -1;
}

void Widget::on_pushButton_Reset_clicked()
{
    // Reset the graph zoom
    plot1.setAxisAutoScale(QwtPlot::yLeft, true);
    plot1.setAxisAutoScale(QwtPlot::xBottom, true);
    plot2.setAxisAutoScale(QwtPlot::xBottom, true);
    plot3.setAxisAutoScale(QwtPlot::xBottom, true);
    plot1.replot();
    plot2.replot();
    plot3.replot();
}

void Widget::on_radioButton_click_clicked(bool checked)
{
    if(checked) {
        disconnect( plotPickerRect, 0, this, 0 );
        disconnect( plotPickerRect2, 0, this, 0 );
        disconnect( plotPickerRect3, 0, this, 0 );
    }
}

void Widget::on_radioButton_drag_clicked(bool checked)
{
    if(checked) {
        connect( plotPickerRect, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );
        connect( plotPickerRect2, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );
        connect( plotPickerRect3, SIGNAL( selected( const QRectF & ) ), this, SLOT(on_rect_dragged( const QRectF & ) ) );
    }
}
void Widget::on_pushButton_getSource_clicked()
{
    QPointF closestPoint;
    qreal y = closestPoint.y();
    qlonglong pcAddress = 2 * y - pcAddressSeperator;
    QString pcAddressHex;
    pcAddressHex = pcAddressHex.number(pcAddress, 16);

    // Use info line *0x<pcAddressHexa > to get the source code at that line
    process->write((QString("list *0x") + pcAddressHex).toLocal8Bit());     // The GDB command
    process->write("\n");
}
